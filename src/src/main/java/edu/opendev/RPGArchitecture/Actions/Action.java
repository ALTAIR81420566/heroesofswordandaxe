package edu.opendev.RPGArchitecture.Actions;
import edu.opendev.RPGArchitecture.Persons.Person;

/**
 * Created by altair on 15.11.16.
 */
public abstract class Action {

    public String name;
    public Action(){

    }

    public abstract void execute(Person attacker, Person defender);
}
