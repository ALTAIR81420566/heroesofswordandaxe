package edu.opendev.RPGArchitecture.Persons;

import edu.opendev.RPGArchitecture.Actions.Action;

import java.awt.*;
import java.util.List;

/**
 * Created by altair on 15.11.16.
 */
public abstract class Person  {

    protected int hp;
    protected int defence;
    protected int damage;
    protected int exp;
    protected List<Action> actionList;
    protected Image image;

    public Person(){

    }

    public abstract void initActions();

    public abstract void levelUp();


}
